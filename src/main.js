import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.ts'

import './assets/style/css/main.css'
// import './assets/library/bs/js/bootstrap.min.js'
// import './assets/library/mdb/js/mdb.min.js'


const app = createApp(App)

app.use(router)

app.mount('#app')
